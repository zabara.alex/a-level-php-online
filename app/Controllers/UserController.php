<?php

namespace App\Controllers;

use App\Models\UserModel;

class UserController extends BaseController
{
    public function index()
    {
        $u = UserModel::test();
        return $this->view('user/index', ['array' => $u]);
    }
}
