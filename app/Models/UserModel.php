<?php

namespace App\Models;

use ZabaraIndustry\Framework\Models\Model;

class UserModel extends Model
{
    protected string $table = 'users';

//    public function userRemove()
//    {
//        $sql = "SELECT * FROM $this->table";
//
//        return $this->pdo->query($sql);
//    }

    public static function test()
    {
        $user = new UserModel();
        return $user->findAll();
    }
}