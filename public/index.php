<?php
require_once '../vendor/autoload.php';
require_once '../packages/zabara_industry/framework/src/fn.php';

use App\Controllers\ProductController;
use App\Controllers\UserController;
use Symfony\Component\Dotenv\Dotenv;
use ZabaraIndustry\Framework\Router;

define('ROOT', dirname(__DIR__));

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

// Laravel 7+
// Route::get('/user', [UserController::class, 'index']);
// Route::get('/user', [UserController::class]);

Router::add('/user', [UserController::class]);
Router::add('/product', [ProductController::class, 'index']);


Router::dispatch($_SERVER['REQUEST_URI']);

