<?php
namespace ZabaraIndustry\Framework;

class Router
{
    protected static array $routes = [];
    protected static array $route = [];

    public static function add(string $url, array $route = []): void
    {
        self::$routes[$url] = $route;
    }

    private static function getRoute(string $url): bool
    {
        if (!empty(self::$routes[$url])) {
            self::$route = self::$routes[$url];
            if (empty(self::$route[1])) {
                self::$route[1] = 'index';
            }
            return true;
        }
        return false;
    }

    public static function dispatch(string $url)
    {
        if (!self::getRoute($url)) {
            http_response_code(404);
            include '404.html';
        }

        $controller = self::$route[0];

        if (!class_exists($controller)) {
            return 'Controller ' . $controller . ' Not Found';
        }

        $cObj = new $controller(self::$route);

        $action = self::$route[1];

        if (method_exists($cObj, $action)) {
            $cObj->$action();
        } else {
            return '<br>Action ' . $action . ' is not Found';
        }
    }
}