<?php
namespace ZabaraIndustry\Framework\Controllers;

use ZabaraIndustry\Framework\Views\View;

class Controller
{
    public string $view;
    public function view(string $view, $data = []): bool
    {
        $this->view = $view;
        $viewObj = new View($this->view);
        $viewObj->render($data);
        return true;
    }
}