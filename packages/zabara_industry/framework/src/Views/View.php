<?php
namespace ZabaraIndustry\Framework\Views;

class View
{
    public array $route = [];
    public string $view;

    public function __construct($view)
    {
        $this->view = $view;
    }

    public function render($data): void
    {
        if (is_array($data)) {
            extract($data);
        }
        $fileView = ROOT . '/app/Views/' . $this->view . '.php';
        if (is_file($fileView)) {
            require $fileView;
        } else {
            echo '<h1> Файл представления не найден</h1>';
        }
    }
}