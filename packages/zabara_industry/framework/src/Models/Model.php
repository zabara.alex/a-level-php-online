<?php

namespace ZabaraIndustry\Framework\Models;

use ZabaraIndustry\Framework\DB;

class Model
{
    protected object $pdo;
    protected string $table;
    public function __construct()
    {
        $this->pdo = DB::getInstance();
    }

    public function findAll()
    {
        $sql = "SELECT * FROM $this->table";

        return $this->pdo->query($sql);
    }

    public function where(string $key, string $compare, string $search): object
    {
        $key = htmlspecialchars($key);
        $compare = htmlspecialchars($compare);
        $search = htmlspecialchars($search);

        $str = $key . $compare . "'" . $search . "'";

        $sql = "SELECT * FROM $this->table WHERE $str";

        return $this->pdo->query($sql);
    }
}