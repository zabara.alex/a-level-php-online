<?php

namespace ZabaraIndustry\Framework;

use \PDO;

class DB
{
    protected object $pdo;

    protected static $instance;

    protected function __construct()
    {
        $db = require_once ROOT . '/config/database.php';
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
//            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ];

        $connect = 'mysql:host='.$db['DB_HOST'] . ';port=' . $db['DB_PORT']
            . ';dbname=' . $db['DB_DATABASE'];

        $this->pdo = new PDO($connect, $db['DB_USERNAME'], $db['DB_PASSWORD'], $options);
    }

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function query(string $sql)
    {
        $PDOStatement = $this->pdo->prepare($sql);
        $result = $PDOStatement->execute();
        if ($result !== false) {
            return $PDOStatement->fetchAll();
        }
        echo 'По данному запросу мы ничего не нашли';
        return [];
    }
}
